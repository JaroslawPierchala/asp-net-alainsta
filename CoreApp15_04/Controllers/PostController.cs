﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CoreApp15_04_Instagram.Models;
using CoreApp15_04_Instagram.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoreApp15_04_Instagram.DAL;

namespace CoreApp15_04_Instagram.Controllers
{
	public class PostController : Controller
	{
		public static List<Post> PublishedPosts { get; set; } = new List<Post>();

		private IHostingEnvironment _env;
		private IAlaInstagramData _alaInstagramData;

		public PostController(IHostingEnvironment env)
		{
			_alaInstagramData = new DbAlaInstagramData();
			_env = env;
		}

		// GET: Post
		public ActionResult Index()
		{
			//Auto Mapper Poszukac info
			PublishedPosts = _alaInstagramData.GetAll().ToList();
			var displayPostViewModels = PublishedPosts.Select(n => new DisplayPostViewModel
			{
				Title = n.Title,
				PhotoPaths = n.CommaSeparatedPhotoPaths.Split(',').ToList(),
				Tags = _alaInstagramData.GetTags(n).Select(x => x.Name).ToList()
			});

			return View(displayPostViewModels);
		}

		// GET: Post/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: Post/Create
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(AddPostViewModel addPostViewModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					// 1. Zapis plik na dysku TAK
					// 2. Zapis post do bazy TAK
					// 3. Przesuwanie zdjec jesli jest przeslane wiecej zdjec TAK/NIE
					// 4. Sposob przeslania wiecej zdjec na raz do jednego posta TAK
					// 5. Wyswietlanie podgladu zdjec przed wyslalem TAK
					// 6. Sposob usuniecia zdjecia z wysylanych w podgladzie NIE

					var uploadFolder = "Upload";
					var savePhotoPath = Path.Combine(_env.WebRootPath, uploadFolder);

					List<string> paths = new List<string>();

					foreach (IFormFile photo in addPostViewModel.Photos)
					{
						string fileName = photo.FileName;
						var savePath = Path.Combine(savePhotoPath, fileName);
						using (var photoSave = new FileStream(savePath, FileMode.Create))
						{
							photo.CopyTo(photoSave);
						}
						paths.Add("/" + uploadFolder + "/" + fileName);
					}

					//TODO: zapis do bazy (z migracjami)
					var newPost = new Post
					{
						Title = addPostViewModel.Title,
						//PostTag = addPostViewModel.CommaSeparatedTags.Split(',')
						//.Select(t => new Tag { Name = t }).ToList(),
						CommaSeparatedPhotoPaths = String.Join(',', paths)
					};

					foreach (var tagName in addPostViewModel.CommaSeparatedTags.Split(','))
					{
						if (_alaInstagramData.GetTagByName(tagName) == null)
						{
							newPost.PostTag.Add(new PostTag
							{
								Post = newPost,
								Tag = new Tag { Name = tagName }
							}
						);
						}
						else
						{
							newPost.PostTag.Add(new PostTag
							{
								Post = newPost,
								TagId = _alaInstagramData.GetTagByName(tagName).Id
							}
						);
						}
						
					}

					PublishedPosts.Add(newPost);
					_alaInstagramData.SaveAlaInstagramPost(newPost);

					return RedirectToAction(nameof(Index));
				}
				return View(addPostViewModel);
			}
			catch (Exception ex)
			{
				return View();
			}
		}
	}
}