﻿using CoreApp15_04_Instagram.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreApp15_04_Instagram.DAL
{
    public class AlaInstagramDbContext : DbContext
    {
		public DbSet<Post> AlaInstagramPosts { get; set; }
		public DbSet<Tag> AlaInstagramTags { get; set; }
		public DbSet<PostTag> AlaInstagramPostTag { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			var connStr = @"Data Source=USER-KOMPUTER\SQLEXPRESS;Database=Alainstagram;Integrated Security=True";

			optionsBuilder.UseSqlServer(connStr);
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<PostTag>().HasKey(sc => new { sc.PostId, sc.TagId });
		}
	}
}
