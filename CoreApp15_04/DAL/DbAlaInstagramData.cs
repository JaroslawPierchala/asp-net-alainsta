﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreApp15_04_Instagram.Models;
using Microsoft.EntityFrameworkCore;

namespace CoreApp15_04_Instagram.DAL
{
	public class DbAlaInstagramData : IAlaInstagramData
	{
		public DbAlaInstagramData()
		{
			using (var context = new AlaInstagramDbContext())
			{
				context.Database.EnsureCreated();
			}
		}

		public IEnumerable<Post> GetAll()
		{
			using (var context = new AlaInstagramDbContext())
			{
				//context.Database.EnsureCreated();

				return context.AlaInstagramPosts.ToList();
			}
		}

		public void SaveAlaInstagramPost(Post AlaInstagramPost)
		{
			using (var context = new AlaInstagramDbContext())
			{
				//context.Database.EnsureCreated();
				context.AlaInstagramPosts.Add(AlaInstagramPost);

				context.SaveChanges();
			}
		}
		public IEnumerable<Tag> GetTags(Post AlaInstagramPost)
		{
			using (var context = new AlaInstagramDbContext())
			{
				//context.Database.EnsureCreated();

				return context.AlaInstagramTags.Where(pt => pt.PostTag.Any(t => t.PostId == AlaInstagramPost.Id)).ToList();
				//return context.AlaInstagramTags.Include(x => x.PostTag).ThenInclude(x => x.Post);
			}
		}

		public Tag GetTagByName(string name)
		{
			using (var context = new AlaInstagramDbContext())
			{
				//context.Database.EnsureCreated();

				return context.AlaInstagramTags.Where(t => t.Name == name).FirstOrDefault();
			}
		}
	}
}
