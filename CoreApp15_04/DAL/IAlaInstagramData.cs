﻿using CoreApp15_04_Instagram.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreApp15_04_Instagram.DAL
{
    public interface IAlaInstagramData
    {
		void SaveAlaInstagramPost(Post AlaInstagramPost);
		IEnumerable<Post> GetAll();
		IEnumerable<Tag> GetTags(Post AlaInstagramPost);
		Tag GetTagByName(string name);
	}
}
