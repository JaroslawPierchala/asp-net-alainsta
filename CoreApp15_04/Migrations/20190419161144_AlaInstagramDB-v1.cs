﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreApp15_04_Instagram.Migrations
{
    public partial class AlaInstagramDBv1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AlaInstagramPosts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    CommaSeparatedPhotoPaths = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlaInstagramPosts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AlaInstagramTags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    PostId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlaInstagramTags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AlaInstagramTags_AlaInstagramPosts_PostId",
                        column: x => x.PostId,
                        principalTable: "AlaInstagramPosts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AlaInstagramTags_PostId",
                table: "AlaInstagramTags",
                column: "PostId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AlaInstagramTags");

            migrationBuilder.DropTable(
                name: "AlaInstagramPosts");
        }
    }
}
