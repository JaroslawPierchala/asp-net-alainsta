﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreApp15_04_Instagram.Models
{
	public class Post
	{
		public Guid Id { get; set; }
		public string Title { get; set; }
		public virtual ICollection<PostTag> PostTag { get; set; }
		public string CommaSeparatedPhotoPaths { get; set; }

		public Post()
		{
			Id = Guid.NewGuid();
			this.PostTag = new HashSet<PostTag>();
		}
	}
}
