﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreApp15_04_Instagram.Models
{
	public class PostTag
	{
		public Guid PostId { get; set; }
		public int TagId { get; set; }

		public Post Post { get; set; }
		public Tag Tag { get; set; }
	}
}
