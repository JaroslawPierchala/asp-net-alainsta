﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreApp15_04_Instagram.Models
{
	public class Tag
	{
		public int Id { get; set; }
		public string Name { get; set; }

		public virtual ICollection<PostTag> PostTag { get; set; }

		public Tag()
		{
			this.PostTag = new HashSet<PostTag>();
		}
	}
}
