﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreApp15_04_Instagram.ViewModels
{
	public class AddPostViewModel
	{
		[Required]
		public string Title { get; set; }
		[Required]
		public string CommaSeparatedTags { get; set; }
		[Required]
		public List<IFormFile> Photos { get; set; }
	}
}
