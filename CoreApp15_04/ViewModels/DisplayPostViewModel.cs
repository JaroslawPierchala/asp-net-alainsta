﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreApp15_04_Instagram.ViewModels
{
	public class DisplayPostViewModel
	{
		public string Title { get; set; }
		public List<string> Tags { get; set; }
		public List<string> PhotoPaths { get; set; }
	}
}
