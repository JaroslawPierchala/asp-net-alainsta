﻿$(document).ready(function () {
    if (window.File && window.FileList && window.FileReader) {
        $("#file").on("change", function (e) {
            var files = e.target.files,
                filesLength = files.length;
            for (var i = 0; i < filesLength; i++) {
                var f = files[i]
                var fileReader = new FileReader();
                fileReader.onload = (function (e) {
                    var file = e.target;
                    $("<div id=\"preview\" class=\"col-sm-6 col-md-4 col-lg-3 col-xl-2 col-12 mb-1\">" +
                        "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                        //"<br/><span class=\"remove btn btn-danger\">Remove image</span>"
                        + "</div>").appendTo("#files");
                    $(".remove").click(function () {
                        $(this).parent("#preview").remove();
                    });

                    // Old code here
                    /*$("<img></img>", {
                      class: "imageThumb",
                      src: e.target.result,
                      title: file.name + " | Click to remove"
                    }).insertAfter("#files").click(function(){$(this).remove();});*/

                });
                fileReader.readAsDataURL(f);
            }
        });
    } else {
        alert("Your browser doesn't support to File API")
    }
});